package com.sejasa.weatherinformation

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Build
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
import com.sejasa.weatherinformation.model.Weather
import com.sejasa.weatherinformation.model.WeatherResp
import kotlinx.android.synthetic.main.activity_main.*
import pub.devrel.easypermissions.EasyPermissions
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    val RC_LOCATION = 99
    var isFahrenheit = false

    var weatherResp = WeatherResp()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        switch_temp.setOnCheckedChangeListener { buttonView, isChecked ->
            isFahrenheit = isChecked

            setTempText()
        }

        open_map.setOnClickListener {
            MapsActivity.start(this)
        }
    }

    override fun onResume() {
        super.onResume()

        getCurrentLocation()
    }

    private fun setTempText() {
        if (isFahrenheit) {
            temp.text = "${kelvinToFahrenheit(weatherResp.main?.temp)} °F"
        } else {
            temp.text = "${kelvinToCelcius(weatherResp.main?.temp)} °C"
        }
    }

    @SuppressLint("MissingPermission")
    fun getCurrentLocation(){

        val perms = Manifest.permission.ACCESS_FINE_LOCATION

        if (EasyPermissions.hasPermissions(this, perms)) {
            // GET MY CURRENT LOCATION
            val mFusedLocation = LocationServices.getFusedLocationProviderClient(this)
            mFusedLocation.lastLocation.addOnSuccessListener(this
            ) { location ->
                setCurrentLocation(location)

                val handler = RestHandler(this)
                handler.getCurrentWeather(location.latitude, location.longitude)
            }
        } else {
            EasyPermissions.requestPermissions(
                this@MainActivity, getString(R.string.location_rationale),
                RC_LOCATION, perms
            )
        }
    }

    private fun setCurrentLocation(location: Location?) {
        Toast.makeText(this@MainActivity,
            "Lat : ${location?.latitude} Long : ${location?.longitude}",
            Toast.LENGTH_LONG).show()

        lat.text = location?.latitude.toString()
        lon.text = location?.longitude.toString()

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        getCurrentLocation()
    }

    fun showLoading(isShow: Boolean) {

    }

    fun initWeather(resp: WeatherResp) {
        weatherResp = resp

        val weather = resp.weather?.get(0)
        val main = resp.main
        val wind = resp.wind

        weather_text.text = weather?.main
        humidity.text = "${main?.humidity} %"
        wind_text.text = wind?.speed.toString()

        setTempText()

        ic_weather.setImageResource(
                getWeatherIcon(weather!!.main!!)
        )
    }

    private fun getWeatherIcon(main: String): Int {
        var drawableId = -99

        with(main.toLowerCase()) {
            when {
                contains("clear") -> drawableId = R.drawable.ic_sunny
                contains("rain") -> drawableId = R.drawable.ic_rain
                contains("cloud") -> drawableId = R.drawable.ic_cloudy
                contains("drizzle") -> drawableId = R.drawable.ic_rain
                contains("snow") -> drawableId = R.drawable.ic_snow
                else -> drawableId = R.drawable.ic_sunny
            }
        }

        return drawableId
    }

    private fun kelvinToCelcius(temp: Double?): String? {
        return DecimalFormat("##.#").format(temp?.minus(273.15))
    }

    private fun kelvinToFahrenheit(temp: Double?): String? {
        return DecimalFormat("##.#").format(temp?.minus(273.15)!! * 9/5 + 3)
    }

}
