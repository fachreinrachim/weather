package com.sejasa.weatherinformation.model

import com.google.android.gms.maps.model.LatLng

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-13.
 */
class CityLatLng(cityName: String, cityLatLng: LatLng) {

    var name : String? = null
    var latLng : LatLng? = null

    init {
        name = cityName
        latLng = cityLatLng
    }
}