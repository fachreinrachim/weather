package com.sejasa.weatherinformation.rest;

import java.util.ArrayList;
import java.util.List;

import com.sejasa.weatherinformation.model.WeatherResp;
import io.reactivex.Observable;
import retrofit2.http.*;

public interface ApiServices {

    String WEATHER = "weather";

    String LAT = "lat";
    String LON = "lon";
    String APP_ID = "appid";

    /**
     * ========== WEATHER ==========
     **/
    @GET(WEATHER)
    Observable<WeatherResp> getWeatherByLatLon(
            @Query(LAT) Double lat,
            @Query(LON) Double lon,
            @Query(APP_ID) String appid
    );
}
