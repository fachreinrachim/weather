package com.sejasa.weatherinformation.model

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */

class Weather() {
    val id: Int? = null
    val main: String? = null
    val description: String? = null
    val icon: String? = null
}