package com.sejasa.weatherinformation.rest

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */
class ExceptionHelper {

    companion object {
        val ERROR_MESSAGE_DEFAULT = "Something error"

        fun getMessage(exception: Throwable): String? {

            val cause = exception.cause ?: return ERROR_MESSAGE_DEFAULT

            return cause.message
        }
    }
}