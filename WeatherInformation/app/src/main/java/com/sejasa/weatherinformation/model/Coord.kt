package com.sejasa.weatherinformation.model

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */
class Coord(aLan : Double, aLon: Double) {
    val lon: Double? = null
    val lat: Double? = null
}