package com.sejasa.weatherinformation

import android.widget.Toast
import com.sejasa.weatherinformation.model.WeatherResp
import com.sejasa.weatherinformation.rest.ApiClient
import com.sejasa.weatherinformation.rest.ApiServices
import com.sejasa.weatherinformation.rest.ExceptionHelper
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */
class RestHandler(val activity: MainActivity) {

    fun getCurrentWeather(lat: Double, lon: Double) {
        activity.showLoading(true)
        val observable = ApiClient.getRxInstance()
                .create(ApiServices::class.java)
                .getWeatherByLatLon(lat, lon, activity.getString(R.string.app_id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())

        observable.subscribe(object : Observer<WeatherResp> {
            override fun onSubscribe(d: Disposable) {}

            override fun onNext(resp: WeatherResp) {
                activity?.apply {
                    showLoading(false)
                    activity.initWeather(resp)
                }
            }

            override fun onError(e: Throwable) {
                e.printStackTrace()
                Toast.makeText(activity, ExceptionHelper.getMessage(e), Toast.LENGTH_LONG).show()
            }

            override fun onComplete() {
                activity.showLoading(false)
            }
        })

    }
}