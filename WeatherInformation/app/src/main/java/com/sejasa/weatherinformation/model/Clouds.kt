package com.sejasa.weatherinformation.model

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */
class Clouds {
    val all: Int? = null
}