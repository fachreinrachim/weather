package com.sejasa.weatherinformation.model

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */
class Wind {
    val speed: Double? = null
    val deg: Int? = null
}