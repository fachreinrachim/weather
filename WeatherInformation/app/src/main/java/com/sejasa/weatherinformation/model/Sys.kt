package com.sejasa.weatherinformation.model

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */
class Sys {
    val message: Double? = null
    val country: String? = null
    val sunrise: Int? = null
    val sunset: Int? = null
}