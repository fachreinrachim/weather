package com.sejasa.weatherinformation

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sejasa.weatherinformation.model.CityLatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.sejasa.weatherinformation.model.WeatherResp
import android.graphics.Bitmap
import androidx.core.content.ContextCompat
import android.graphics.Canvas
import android.graphics.Color
import com.google.android.gms.maps.model.BitmapDescriptor
import java.text.DecimalFormat
import android.widget.TextView
import android.graphics.Typeface
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import com.google.android.gms.maps.model.Marker




class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    lateinit var citiesLatLon : ArrayList<CityLatLng>
    lateinit var weatherList : ArrayList<WeatherResp>

    companion object {

        fun start(context: Context) {
            val intent = Intent(context, MapsActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        weatherList = ArrayList()

        setupMap()

        initCitiesLatLng()

        getWeatherList()
    }

    private fun getWeatherList() {
        val handler = MapsWeatherHandler(this)
        citiesLatLon.forEach {
            handler.getCurrentWeather(it)
        }
    }

    private fun setupMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun initCitiesLatLng() {
        citiesLatLon = ArrayList()

        citiesLatLon.add(CityLatLng("Bandung", LatLng(-6.914744, 107.609810)))
        citiesLatLon.add(CityLatLng("Jakarta", LatLng(-6.200000, 106.816666)))
        citiesLatLon.add(CityLatLng("Serang", LatLng(-6.120000, 106.150276)))
        citiesLatLon.add(CityLatLng("Semarang", LatLng(-6.966667, 110.416664)))
        citiesLatLon.add(CityLatLng("Yogyakarta", LatLng(-7.797068, 110.370529)))
        citiesLatLon.add(CityLatLng("Surabaya", LatLng(-7.250445, 112.768845)))
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    fun initMap() {
        val builder = LatLngBounds.Builder()
        weatherList.forEach {
            mMap.addMarker(
                    MarkerOptions().position(it.cityLatLng!!.latLng!!)
                            .title(it.cityLatLng!!.name)
                            .snippet(getSnippetWeather(it))
                            .icon(bitmapDescriptorFromVector(
                                    this, getWeatherIcon(it.weather!![0].main!!)
                            ))
            )

            builder.include(it.cityLatLng!!.latLng)
        }

        val bounds = builder.build()
        val padding = 64 // offset from edges of the map in pixels
        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)

        mMap.animateCamera(cu)

        mMap.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {

            override fun getInfoWindow(arg0: Marker): View? {
                return null
            }

            override fun getInfoContents(marker: Marker): View {

                val context = applicationContext //or getActivity(), YourActivity.this, etc.

                val info = LinearLayout(context)
                info.orientation = LinearLayout.VERTICAL

                val title = TextView(context)
                title.setTextColor(Color.BLACK)
                title.gravity = Gravity.CENTER
                title.setTypeface(null, Typeface.BOLD)
                title.text = marker.title

                val snippet = TextView(context)
                snippet.setTextColor(Color.GRAY)
                snippet.text = marker.snippet

                info.addView(title)
                info.addView(snippet)

                return info
            }
        })
    }

    private fun getSnippetWeather(weatherResp: WeatherResp): String? {
        return weatherResp.weather!![0].main +
                "\n" +
                getString(R.string.temp) + " : " + "${kelvinToCelcius(weatherResp.main?.temp)} °C" +
                "\n" +
                getString(R.string.humidity) + " : " + "${weatherResp.main?.humidity} %" +
                "\n" +
                getString(R.string.wind) + " : " + weatherResp.wind?.speed.toString()
    }

    fun showLoading(isShow: Boolean) {

    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap = Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun addWeather(resp: WeatherResp) {
        weatherList.add(resp)

        initMap()
    }

    private fun getWeatherIcon(main: String): Int {
        var drawableId = -99

        with(main.toLowerCase()) {
            when {
                contains("clear") -> drawableId = R.drawable.ic_sunny
                contains("rain") -> drawableId = R.drawable.ic_rain
                contains("cloud") -> drawableId = R.drawable.ic_cloudy
                contains("drizzle") -> drawableId = R.drawable.ic_rain
                contains("snow") -> drawableId = R.drawable.ic_snow
                else -> drawableId = R.drawable.ic_sunny
            }
        }

        return drawableId
    }

    private fun kelvinToCelcius(temp: Double?): String? {
        return DecimalFormat("##.#").format(temp?.minus(273.15))
    }
}
