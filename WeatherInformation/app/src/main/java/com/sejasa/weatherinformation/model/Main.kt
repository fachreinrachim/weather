package com.sejasa.weatherinformation.model

import com.google.gson.annotations.SerializedName

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */
class Main {
    val temp: Double? = null
    val pressure: Double? = null
    val humidity: Int? = null

    @SerializedName("temp_min")
    val tempMin: Double? = null

    @SerializedName("temp_max")
    val tempMax: Double? = null

    @SerializedName("sea_level")
    val seaLevel: Double? = null

    @SerializedName("grnd_level")
    val grndLevel: Double? = null
}