package com.sejasa.weatherinformation.model

/**
 * @author M.Fachrein Rachim <fachreinrachim@gmail.com>
 * @since 2019-07-12.
 */
class WeatherResp {
    val coord: Coord? = null
    val weather: List<Weather>? = null
    val base: String? = null
    val main: Main? = null
    val wind: Wind? = null
    val clouds: Clouds? = null
    val dt: Int? = null
    val sys: Sys? = null
    val id: Int? = null
    val name: String? = null
    val cod: Int? = null

    var cityLatLng: CityLatLng? = null
}